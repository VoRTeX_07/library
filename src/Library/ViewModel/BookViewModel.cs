﻿using System;

namespace Library.ViewModel
{
    public class BookViewModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public int PagesCount { get; set; }

        public DateTime PublicationDate { get; set; }

        public DateTime CreateDate { get; set; }

        public int Price { get; set; }
    }
}