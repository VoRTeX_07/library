﻿using System;

namespace Library.ViewModel
{
    public class CategoryViewModel
    {
        public Guid Id { get; set; }
    
        public  string Name { get; set; }
    }
}