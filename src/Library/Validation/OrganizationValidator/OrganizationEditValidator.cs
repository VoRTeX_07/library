﻿using FluentValidation;
using Library.ViewModels.OrganizationModels;

namespace Library.Validation.OrganizationValidator
{
    public class OrganizationEditValidator: AbstractValidator<OrganizationEditModel>
    {
        public OrganizationEditValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Books).NotEmpty();
            RuleFor(x => x.Title).NotEmpty().Length(1, 100)
                .WithMessage("Please enter the Title, max lenght 100 Symbols");
            RuleFor(x => x.Descriprion).Length(500).WithMessage("Max lenght 500 symbols ");
        }
    }
}