﻿using FluentValidation;
using Library.ViewModels.OrganizationModels;

namespace Library.Validation.OrganizationValidator
{
    public class OrganizationCreateValidator:AbstractValidator<OrganizationCreateModel>
    {
        public OrganizationCreateValidator()
        {
            RuleFor(x => x.Title).NotEmpty().Length(1, 100).WithMessage("Please enter the Title, max lengt 100 symbols");
        }
    }
}