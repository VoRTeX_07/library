﻿using FluentValidation;
using Library.ViewModels.AuthorModels;

namespace Library.Validation.AuthorValidator
{
    public class AuthorCreateValidator:AbstractValidator<AuthorCreateModel>
    {
        public AuthorCreateValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().Length(1, 50)
                .WithMessage("Please enter the First name, max lenght 50 symbols");
            RuleFor(x => x.SecondName).NotEmpty().Length(1, 50)
                .WithMessage("Please enter the Second name, max lenght 50 symbols");
            RuleFor(x => x.MiddleName).NotEmpty().Length(1, 50)
                .WithMessage("Please enter the Middle name, max length 50 symbols");
        }
    }
}