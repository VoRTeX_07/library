﻿using FluentValidation;
using Library.ViewModels.BookModels;

namespace Library.Validation.BookValidator
{
    public class BookCreateValidator: AbstractValidator<BookCreateModel>
    {
        public BookCreateValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Please enter the Title").Length(1,100);
        }
    }
}