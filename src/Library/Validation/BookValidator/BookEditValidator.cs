﻿using FluentValidation;
using Library.ViewModels.BookModels;

namespace Library.Validation.BookValidator
{
    public class BookEditValidator:AbstractValidator<BookEditModel>
    {
        public BookEditValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Price).NotEmpty().WithMessage("Please enter the Price");
            RuleFor(x => x.Title).NotEmpty().Length(1,100).WithMessage("Please enter the Title, Max lenght 100 symbols");
            RuleFor(x => x.PagesCount).NotEmpty().WithMessage("Please enter the pages count");
        }
    }
}