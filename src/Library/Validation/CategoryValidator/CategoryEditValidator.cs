﻿using FluentValidation;
using Library.ViewModels.CategoryModels;

namespace Library.Validation.CategoryValidator
{
    public class CategoryEditValidator: AbstractValidator<CategoryEditModel>
    {
        public CategoryEditValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Name).NotEmpty().Length(1, 100).WithMessage("Please enter the name,max length 100 symbols");
        }
    }
}