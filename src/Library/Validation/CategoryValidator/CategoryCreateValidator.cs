﻿using FluentValidation;
using Library.ViewModel.CategoryModels;

namespace Library.Validation.CategoryValidator
{
    public class CategoryCreateValidator:AbstractValidator<CategoryCreateModel>
    {
        public CategoryCreateValidator()
        {
            RuleFor(x => x.Name).NotEmpty().Length(1, 100).WithMessage("Please enter the Name, max lenght 100 symbols");
        }
    }
}