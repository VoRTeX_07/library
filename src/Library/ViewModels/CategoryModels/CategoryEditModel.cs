﻿using System;

namespace Library.ViewModels.CategoryModels
{
    public class CategoryEditModel
    {
        public Guid Id { get; set; }
    
        public  string Name { get; set; }
    }
}