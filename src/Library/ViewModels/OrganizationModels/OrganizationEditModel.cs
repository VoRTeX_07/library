﻿using System;
using Library.ViewModels.BookModels;

namespace Library.ViewModels.OrganizationModels
{
    public class OrganizationEditModel
    {
        public Guid Id { get; set; }
        
        public string Title { get; set; }
        
        public string Descriprion { get; set; }
        
        public BookEditModel Books { get; set; }
    }
}