﻿using System;

namespace Library.ViewModels.BookModels
{
    public class BookEditModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int PagesCount { get; set; }

        public DateTime PublicationDate { get; set; }

        public DateTime CreateDate { get; set; }

        public int Price { get; set; }
    }
}