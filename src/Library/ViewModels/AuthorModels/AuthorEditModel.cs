﻿using System;

namespace Library.ViewModels.AuthorModels
{
    public class AuthorEditModel
    {
        public Guid Id { get; set; }

        public  string FirstName { get; set; }
        
        public string SecondName { get; set; }
        
        public string MiddleName { get; set; }
        
        public string Biography { get; set; }
    }
}