﻿
namespace Library.ViewModels.AuthorModels
{
    public class AuthorCreateModel
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string MiddleName { get; set; }
    }
}