﻿using System;
using Library.ViewModel;

namespace Library.ViewModels
{
    public class OrganizationViewModel
    {
        public Guid Id { get; set; }
        
        public string Title { get; set; }
        
        public string Descriprion { get; set; }
        
        public BookViewModel Books { get; set; }
    }
}