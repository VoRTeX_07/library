﻿using Library.DL.Models;
using Library.ViewModel;
using Library.ViewModels.BookModels;

namespace Library.Profile
{
    public class BookViewModelProfile: AutoMapper.Profile
    {
        public BookViewModelProfile()
        {
            CreateMap<Book, BookCreateModel>()
                .ReverseMap();

            CreateMap<Book, BookEditModel>()
                .ReverseMap();

            CreateMap<Book, BookViewModel>()
                .ReverseMap();
        }
    }
}