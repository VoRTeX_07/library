﻿using Library.DL.Models;
using Library.ViewModels;
using Library.ViewModels.AuthorModels;

namespace Library.Profile
{
    public class AuthorViewModelProfile: AutoMapper.Profile
    {
        public AuthorViewModelProfile()
        {
            CreateMap<Author, AuthorCreateModel>()
                .ReverseMap();

            CreateMap<Author, AuthorViewModel>()
                .ForMember(x => x.FullName, o => 
                    o.MapFrom(p => ($"{p.SecondName} {p.FirstName} {p.MiddleName}")));
                

            CreateMap<Author, AuthorEditModel>()
                .ReverseMap();
        }
    }
}