﻿using Library.DL.Models;
using Library.ViewModels;
using Library.ViewModels.OrganizationModels;

namespace Library.Profile
{
    public class OrganizationViewModelProfile : AutoMapper.Profile
    {
        public OrganizationViewModelProfile()
        {
            CreateMap<Organization, OrganizationCreateModel>()
                .ReverseMap();

            CreateMap<Organization, OrganizationEditModel>()
                .ReverseMap();

            CreateMap<Organization, OrganizationViewModel>()
                .ReverseMap();
        }
    }
}