﻿using Library.DL.Models;
using Library.ViewModel;
using Library.ViewModel.CategoryModels;
using Library.ViewModels.CategoryModels;

namespace Library.Profile
{
    public class CategoryViewModelProfile:AutoMapper.Profile
    {
        public CategoryViewModelProfile()
        {
            CreateMap<Category, CategoryCreateModel>()
                .ReverseMap();

            CreateMap<Category, CategoryEditModel>()
                .ReverseMap();

            CreateMap<Category, CategoryViewModel>()
                .ReverseMap();
        }
    }
}