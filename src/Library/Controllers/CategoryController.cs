﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.DL.Models;
using Library.Helper;
using Library.ViewModel;
using Library.ViewModel.CategoryModels;
using Library.ViewModels.CategoryModels;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly IService<Category> _categoryService;
        private readonly IMapper _mapper;

        public CategoryController(IService<Category> categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        /// <summary>
        /// Get list of categories.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
       [HttpGet]
       [Route("[action]")]
        public async Task<IActionResult> GetAllAsync([FromQuery]int page,[FromQuery]int size)
        {
            var result = PagingHelper.ChekPagingOptions(page, size);
            var allCategory = await _categoryService.GetAllAsync(result.Page,result.Size);
            var listCategory = _mapper.Map<List<CategoryViewModel>>(allCategory);
            return Ok(listCategory);
        }
        /// <summary>
        /// Create new category.
        /// </summary>
        /// <param name="categoryCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> PostAsync([FromBody]CategoryCreateModel categoryCreateModel)
        {
            var item = _mapper.Map<Category>(categoryCreateModel);
            var newCategory =await _categoryService.CreateAsync(item);
            if (newCategory == null)
            {
                return BadRequest();
            }
            var viewCategory = _mapper.Map<CategoryViewModel>(newCategory);
            return Ok(viewCategory);
        }
        /// <summary>
        /// Delete category.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var category =await _categoryService.GetAsync(id);
            if (category == null)
            {
                return NotFound("Ошибка - задача отсутствует.");
            }

            await _categoryService.DeleteAsync(category);
            return Ok();
        }
        /// <summary>
        /// Edit category.
        /// </summary>
        /// <param name="categoryEditModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("[action]")]
        public async Task<IActionResult> PutAsync([FromBody]CategoryEditModel categoryEditModel)
        {
           
            var categoryEdit = await _categoryService.GetAsync(categoryEditModel.Id);
            if (categoryEdit == null)
            {
                return NotFound("Ошибка-категория не обновлена");
            }
            var item = _mapper.Map<CategoryEditModel, Category>(categoryEditModel, categoryEdit);
            await _categoryService.UpdateAsync(item);
            return Ok();
            
        }
    }
}