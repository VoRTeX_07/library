﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.DL.Models;
using Library.Helper;
using Library.ViewModels;
using Library.ViewModels.AuthorModels;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    public class AuthorController:ControllerBase
    {
        private readonly IService<Author> _authorService;
        private readonly IMapper _mapper;
        
        public AuthorController(IService<Author> authorService, IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all authors.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> GetAllAsync([FromQuery]int page,[FromQuery]int size)
        {
            var result = PagingHelper.ChekPagingOptions(page, size);
            var allAuthors =await _authorService.GetAllAsync(result.Page,result.Size);
            var listAuthors = _mapper.Map<List<AuthorViewModel>>(allAuthors);
            return Ok(listAuthors);
        }
        /// <summary>
        /// Create a new author.
        /// </summary>
        /// <param name="authorCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> PostAsync([FromBody]AuthorCreateModel authorCreateModel)
        {
            var item = _mapper.Map<Author>(authorCreateModel);
            var newAuthor = await _authorService.CreateAsync(item);
            {
                if (newAuthor == null)
                {
                    return BadRequest();
                }
            }
            var viewAuthor = _mapper.Map<AuthorViewModel>(newAuthor);
            return Ok(viewAuthor);
        }
        /// <summary>
        /// Delete author.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAsync([FromQuery]Guid id)
        {
            var author =await _authorService.GetAsync(id);
             if (author == null)
             { 
                 return NotFound("Ошибка - задача отсутствует.");
             }

             await _authorService.DeleteAsync(author); 
             return Ok();
        }
        /// <summary>
        /// Edit author.
        /// </summary>
        /// <param name="authorEditModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("[action]")]
        public async Task<IActionResult> PutAsync([FromBody]AuthorEditModel authorEditModel)
        {
            var authorEdit =await _authorService.GetAsync(authorEditModel.Id);
            if (authorEdit == null)
            {
                return NotFound("Ошибка-автор не обновлен");
            }
            var item = _mapper.Map<AuthorEditModel, Author>(authorEditModel, authorEdit);
            await _authorService.UpdateAsync(item);
            return Ok();
        }
    }
}