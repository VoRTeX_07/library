﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.DL.Models;
using Library.Helper;
using Library.ViewModels;
using Library.ViewModels.OrganizationModels;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    public class OrganizationController : ControllerBase
    {
        private readonly IService<Organization> _organizationService;
        private readonly IMapper _mapper;

        public OrganizationController(IService<Organization> organizationService,IMapper mapper)
        {
            _organizationService = organizationService;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all organization.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public  async Task<IActionResult> GetAllAsync([FromQuery]int page,[FromQuery] int size)
        {
            var result = PagingHelper.ChekPagingOptions(page, size);
            var allOrganization =await _organizationService.GetAllAsync(result.Page,result.Size);
            var listOrganization = _mapper.Map<List<OrganizationViewModel>>(allOrganization);
            return Ok(listOrganization);
        }
        /// <summary>
        /// Create a new organization.
        /// </summary>
        /// <param name="organizationCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> PostAsync([FromBody]OrganizationCreateModel organizationCreateModel)
        {
            var item = _mapper.Map<Organization>(organizationCreateModel);
            var newOrganization =await _organizationService.CreateAsync(item);
            if (newOrganization == null)
            {
                return BadRequest();
            }
            return Ok(newOrganization);
        }
        /// <summary>
        /// Delete organization.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var organization = await _organizationService.GetAsync(id);
            if (organization == null)
            { 
                return NotFound("Ошибка - задача отсутствует.");
            }
            await _organizationService.DeleteAsync(organization); 
            return Ok();
            }
        /// <summary>
        /// Edit organization.
        /// </summary>
        /// <param name="organizationEditModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("[action]")]
        public async Task<IActionResult> PutAsync([FromBody]OrganizationEditModel organizationEditModel)
        {
            var organizationEdit = await _organizationService.GetAsync(organizationEditModel.Id);
            if (organizationEdit == null)
            {
                return NotFound("Ошибка-организация не обновлена");
            }
            var item = _mapper.Map<OrganizationEditModel, Organization>(organizationEditModel, organizationEdit);
            await _organizationService.UpdateAsync(item);
            return Ok();
        }
    }
}