﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.DL.Models;
using Library.Helper;
using Library.ViewModel;
using Library.ViewModels.BookModels;
using Microsoft.AspNetCore.Mvc;


namespace Library.Controllers
{
    [Route("api/[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IService<Book> _bookService;
        private readonly IMapper _mapper;

        public BookController(IService<Book> bookService, IMapper mapper)
        {
            _bookService = bookService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Get all book.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> GetAllAsync([FromQuery]int page, [FromQuery]int size)
        {
            var result = PagingHelper.ChekPagingOptions(page, size);
            var allBooks =await _bookService.GetAllAsync(result.Page,result.Size);
            var listView = _mapper.Map<List<BookViewModel>>(allBooks);
            return Ok(listView);
        }
        /// <summary>
        /// Create a new book.
        /// </summary>
        /// <param name="bookCreateModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> PostAsync([FromBody]BookCreateModel bookCreateModel)
        {
            try
            {
                var item = _mapper.Map<Book>(bookCreateModel);
                var newBook = await _bookService.CreateAsync(item);
                if(newBook==null)
                {
                    return BadRequest();
                }
                var viewBook = _mapper.Map<BookViewModel>(newBook);
                return Ok(viewBook);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Delete book.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("[action]")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var book =await _bookService.GetAsync(id);
            if (book == null)
            { 
                return NotFound("Ошибка - задача отсутствует.");
            }
            await _bookService.DeleteAsync(book); 
            return Ok();
        }

        /// <summary>
        /// Edit book.
        /// </summary>
        /// <param name="bookEditModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("[action]")]
        public async Task<IActionResult> PutAsync([FromBody]BookEditModel bookEditModel)
        {
           
            var bookEdit = await _bookService.GetAsync(bookEditModel.Id);
            if (bookEdit == null)
            {
                return NotFound("Ошибка-книга не обновлена");
            }
            var item = _mapper.Map<BookEditModel, Book>(bookEditModel, bookEdit);
            await _bookService.UpdateAsync(item);
            return Ok();
        }
    }
}