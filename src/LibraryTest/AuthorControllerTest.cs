﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.Controllers;
using Library.DL.Models;
using Library.Profile;
using Library.ViewModels.AuthorModels;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace LibraryTest
{
    public class AuthorControllerTest
    {
        private readonly AuthorController _controller;
        private readonly Mock<IService<Author>> _mock;

        public AuthorControllerTest()
        {
            _mock = new Mock<IService<Author>>();
            _controller = new AuthorController(_mock.Object,
                new MapperConfiguration(map => { map.AddProfile(new AuthorViewModelProfile()); }).CreateMapper());
        }

        //Checking Get method for positive result. 
        [Theory]
        [InlineData(1, 1)]
        public async Task Get_GetAllAuthors_ReturnsOkResult(int page, int size)
        {
            //Arrange
            _mock.Setup(author => author.GetAllAsync(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(new List<Author>());
            //Act
            var result = await _controller.GetAllAsync(page, size);
            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        //Checking Post method for not null.
        [Fact]
        public async Task Post_PostNewAuthor_ReturnsOK()
        {
            //Arrange
            _mock.Setup(a => a.CreateAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.PostAsync(new AuthorCreateModel());
            //Assert
            Assert.NotNull(result);
        }

        //Checking Post method for Bad request result.
        [Fact]
        public async Task Post_PostNewAuthor_ReturnsBadRequest()
        {
            //Arrange
            _mock.Setup(author => author.CreateAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.PostAsync(null);
            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        //Checking Delete method for not found result.
        [Theory]
        [InlineData("562c6322-c056-419c-b288-66a527c02a89")]
        public async Task Delete_DeleteAuthor_ReturnsNotFoundResult(Guid id)
        {
            //Arrange
            _mock.Setup(author => author.DeleteAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.DeleteAsync(id);
            //Assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        //Checking Delete method for a positive result.
        [Fact]
        public async Task Delete_DeleteItem_ReturnsOkObjectResult()
        {
            //Arrange
            _mock.Setup(repo => repo.DeleteAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.DeleteAsync(new Guid("562c6322-c056-419c-b288-66a527c02a88"));
            //Assert
            Assert.IsNotType<NotFoundResult>(result);
        }

        //Checking Put method for not found object result.
        [Fact]
        public async Task Put_UpdateAuthorItem_ReturnsNotFoundObjectResult()
        {
            //
            var model = new AuthorEditModel()
            {
                Id = Guid.Empty
            };
            _mock.Setup(repo => repo.UpdateAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.PutAsync(model);
            //Assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        //Checking Put method for a positive result.
        [Fact]
        public async Task Put_PutAuthorItem_ReturnOkResult()
        {
            //Arrange
            _mock.Setup(author => author.UpdateAsync(It.IsAny<Author>()));
            //Act
            var result = await _controller.PutAsync(new AuthorEditModel());
            //Assert
            Assert.IsNotType<OkObjectResult>(result);
        }
    }
}