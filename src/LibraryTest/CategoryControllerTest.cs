﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.Controllers;
using Library.DL.Models;
using Library.Profile;
using Library.ViewModel.CategoryModels;
using Library.ViewModels.CategoryModels;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace LibraryTest
{
    public class CategoryControllerTest
    {
        private readonly CategoryController _controller;
        private readonly Mock<IService<Category>> _mock;

        public CategoryControllerTest()
        {
            _mock = new Mock<IService<Category>>();
            _controller = new CategoryController(_mock.Object,
                new MapperConfiguration(map =>
                {
                    map.AddProfile(new CategoryViewModelProfile());
                }).CreateMapper());
        }

        // Checking the Get method for positive result.
        [Theory]
        [InlineData(1, 1)]
        public async Task Get_GetAllCategory_ReturnsOkResult(int page, int size)
        {
            //Arrange
            _mock.Setup(category => category.GetAllAsync(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(new List<Category>());
            //Act
            var result = await _controller.GetAllAsync(page, size);
            //Assert
            Assert.IsType<OkObjectResult>(result);
        }
        
        [Fact]
        public async Task Post_PostNewCategory_ReturnsOK()
         {
          //Arrange
          _mock.Setup(a => a.CreateAsync(It.IsAny<Category>()));
          //Act
          var result = await _controller.PostAsync(new CategoryCreateModel());
          //Assert
          Assert.NotNull(result);
         }

        //Checking Post method for bar request result.
        [Fact]
        public async Task Post_PostNewCategory_ReturnsBadRequest()
        {
            //Arrange
            _mock.Setup(category => category.CreateAsync(It.IsAny<Category>()));
            //Act
            var result = await _controller.PostAsync(null);
            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        //Checking method for not found object result.
        [Theory]
        [InlineData("562c6322-c056-419c-b288-66a527c02a89")]
        public async Task Delete_DeleteItem_ReturnsNotFoundResult(Guid id)
        {
            //Arrange
            _mock.Setup(category => category.DeleteAsync(It.IsAny<Category>()));
            //Act
            var result = await _controller.DeleteAsync(id);
            //Assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        //Checking Delete method for a positive result.
        [Fact]
               public async Task Delete_DeleteItem_ReturnsOkObjectResult()
             {
                 //Arrange
                 _mock.Setup(repo => repo.DeleteAsync(It.IsAny<Category>()));
                 //Act
                 await _controller.DeleteAsync(new Guid("562c6322-c056-419c-b288-66a527c02a88"));
                 //Assert
                 Assert.IsNotType<OkObjectResult>(null);
             }

               //Checking Put method for not found result.
            [Fact]
            public async Task Put_UpdateCategoryItem_ReturnsNotFoundObjectResult()
            {
                //Arrange
                var model = new CategoryEditModel
                {
                    Id = Guid.Empty
                };
                _mock.Setup(repo => repo.UpdateAsync(It.IsAny<Category>()));
                //Act
                var result = await _controller.PutAsync(model);
                //Assert
                Assert.IsType<NotFoundObjectResult>(result);
            }

            //Checking Put method for positive result.
            [Fact]
            public async Task Put_PutCategoryItem_ReturnsOkResult()
            {
                //Arrange
                _mock.Setup(category => category.UpdateAsync(It.IsAny<Category>()));
                //Act
                var result = await _controller.PutAsync(new CategoryEditModel());
                //Assert
                Assert.IsNotType<NotFoundResult>(result);
            }
        }
    }
