using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.Controllers;
using Library.DL.Models;
using Library.Profile;
using Library.ViewModels.BookModels;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;

namespace LibraryTest
{
    public class BookControllerTest
    {
        private readonly BookController _controller;
        private readonly Mock<IService<Book>> _mock;

        public BookControllerTest()
        {
            _mock = new Mock<IService<Book>>();
            _controller = new BookController(_mock.Object,
                new MapperConfiguration(map =>
                {
                    map.AddProfile(new BookViewModelProfile());
                }).CreateMapper());
        }
        
        // Checking the Get method for positive result. 
         [Theory]
         [InlineData(1,1)]
         public async Task Get_GetAllBooks_ReturnsAllBooks(int page, int size)
         {
             //Arrange
             _mock.Setup(repo => repo.GetAllAsync(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(new List<Book>());
             //Act
             var result = await _controller.GetAllAsync(page, size);
             //Assert
             Assert.IsType<OkObjectResult>(result);
         }
         
         //Checking Post method for positive result.
         [Fact]
        public async Task Post_PostNewBook_ReturnsOK()
         {
          //Arrange
          var model = new BookCreateModel
          {
              Title = "ss"
          };
          _mock.Setup(a => a.CreateAsync(It.IsAny<Book>()));
          //Act
          var result = await _controller.PostAsync(model);
          //Assert 
          Assert.NotNull(result);
         }
        
       // Checking the Post method for a bad request.
        [Fact] 
       public async Task Post_PostNewBook_ReturnsBadRequest()
       {
           //Arrange
           _mock.Setup(repo => repo.CreateAsync(It.IsAny<Book>()));
           //Act
           var result =  await _controller.PostAsync(null);
           
           //Assert
           Assert.IsType<BadRequestResult>(result);
       }

      //Checking Delete method for not found result.
      [Fact]
      public async Task Delete_DeleteItem_ReturnsNotFoundObjectResult()
       {
           //Arrange
           _mock.Setup(repo => repo.DeleteAsync(It.IsAny<Book>()));
           //Act
           var result = await _controller.DeleteAsync(Guid.Empty);
           //Assert
           Assert.IsType<NotFoundObjectResult>(result);
       }

      //Checking Delete method for positive result.
       [Fact]
       public async Task Delete_DeleteItem_ReturnsOkObjectResult()
       {
           //Arrange
           _mock.Setup(repo => repo.DeleteAsync(It.IsAny<Book>()));
           //Act
           var result = await _controller.DeleteAsync(new Guid("562c6322-c056-419c-b288-66a527c02a88"));
           //Assert
           Assert.IsNotType<NotFoundResult>(result);
       }

       //Checking Put method for positive result.
       [Fact]
       public async Task Put_UpdateBookItem_ReturnsOkResult()
       {
           _mock.Setup(repo => repo.UpdateAsync(It.IsAny<Book>()));
           //Act
           var result = await _controller.PutAsync(new BookEditModel());
           //Assert
           Assert.IsNotType<NotFoundResult>(result);
       }

       //Checking Put method for not found result.
      [Fact]
     public async Task Put_UpdateBookItem_ReturnsNotFoundResult()
     {
         //Arrange
         var model = new BookEditModel()
         {
             Id = Guid.Empty
         };
         _mock.Setup(book => book.UpdateAsync(It.IsAny<Book>()));
         //Act
         var result = await _controller.PutAsync(model);
         //Assert
         Assert.IsType<NotFoundObjectResult>(result);
     }
    }
}