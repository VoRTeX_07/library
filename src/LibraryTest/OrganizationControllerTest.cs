﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Library.BL.Interface;
using Library.Controllers;
using Library.DL.Models;
using Library.Profile;
using Library.ViewModels.OrganizationModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using Xunit;

namespace LibraryTest
{
    public class OrganizationControllerTest
    {
        private readonly OrganizationController _controller;
        private readonly Mock<IService<Organization>> _mock;

        public OrganizationControllerTest()
        {
            _mock = new Mock<IService<Organization>>();
            _controller = new OrganizationController(_mock.Object,
                new MapperConfiguration(map => { map.AddProfile(new OrganizationViewModelProfile()); }).CreateMapper());
        }

        //Checking Get method for positive result.
        [Theory]
        [InlineData(1, 1)]
        public async Task Get_GetAllOrganizations_ReturnsOkResult(int page, int size)
        {
            //Arrange
            _mock.Setup(organization => organization.GetAllAsync(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(new List<Organization>());
            //Act
            var result = await _controller.GetAllAsync(page, size);
            //Assert
            Assert.IsType<OkObjectResult>(result);
        }
        
        //Checking Post method for a positive result.
        [Fact]
        public async Task Post_PostNewOrganization_ReturnsOK()
         {
          //Arrange
          _mock.Setup(a => a.CreateAsync(It.IsAny<Organization>()));
          //Act
          var result = await _controller.PostAsync(new OrganizationCreateModel());
          //Assert
          Assert.NotNull(result);
         }

        //Checking Post method for a bad request.
        [Fact]
        public async Task Post_PostNewOrganization_ReturnsBadRequest()
        {
            //Arrange
            _mock.Setup(organization => organization.CreateAsync(It.IsAny<Organization>()));
            //Act
            var result = await _controller.PostAsync(null);
            //Assert
            Assert.IsType <BadRequestResult>(result);
        }

        //Checking Delete method for not found result.
        [Theory]
        [InlineData("562c6322-c056-419c-b288-66a527c02a89")]
        public async Task Delete_DeleteItem_ReturnsNotFoundResult(Guid id)
        {
            //Arrange
            _mock.Setup(organization => organization.DeleteAsync(It.IsAny<Organization>()));
            //Act
            var result = await _controller.DeleteAsync(id);
            //Assert
            Assert.IsType<NotFoundObjectResult>(result);

        }

        //Checking Delete method for a positive result.
        [Fact]
             public async Task Delete_DeleteItem_ReturnsOkObjectResult()
             {
                 //Arrange
                 _mock.Setup(repo => repo.DeleteAsync(It.IsAny<Organization>()));
                 //Act
                 var result = await _controller.DeleteAsync(new Guid("562c6322-c056-419c-b288-66a527c02a88"));
                 //Assert
                 Assert.IsNotType<NotFoundObjectResult>(result);
             }

             //Checking Put method for not found result.
             [Fact]
            public async Task Put_UpdateOrganizationItem_ReturnsBadRequest()
            {
                //Arrange
                var model = new OrganizationEditModel
                {
                    Id = Guid.Empty
                };
                _mock.Setup(repo => repo.UpdateAsync(It.IsAny<Organization>()));
                //Act
                var result = await _controller.PutAsync(model);
                //Assert
                Assert.IsType<NotFoundResult>(result);
            }

            //Checking Put method for a positive result.
            [Fact]
            public async Task Put_PutOrganizationItem_ReturnsOkResult()
            {
                //Arrange
                _mock.Setup(organization => organization.UpdateAsync(It.IsAny<Organization>()));
                //Act
                var result = await _controller.PutAsync(new OrganizationEditModel());
                //Assert
                Assert.IsNotType<NotFoundResult>(result);
            }
        }
    }
