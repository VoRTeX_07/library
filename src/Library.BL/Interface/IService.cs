﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.BL.Interface
{
    public interface IService<T>
    {
        Task<T> CreateAsync(T essence);
        Task DeleteAsync(T essence);
        Task UpdateAsync(T essence);
        Task<List<T>> GetAllAsync(int page, int size);
        Task<T> GetAsync(Guid id);
    }
}
