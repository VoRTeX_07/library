﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.BL.Interface;
using Library.DL;
using Library.DL.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.BL.Repository
{
    public class AuthorRepository : IService<Author>
    {
        private readonly DataContext _dataContext;

        public AuthorRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        /// <summary>
        /// Create new author.
        /// </summary>
        /// <param name="author"></param>
        /// <returns></returns>
        public async Task<Author> CreateAsync(Author author)
        {
            var newAuthor =await _dataContext.Authors.AddAsync(author); 
            await _dataContext.SaveChangesAsync();
            return newAuthor.Entity;
        }

        /// <summary>
        /// Delete essence author from data base.
        /// </summary>
        /// <param name="author"></param>
        public async Task DeleteAsync(Author author)
        {
            _dataContext.Authors.Remove(author);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Update essence author in data base. 
        /// </summary>
        /// <param name="author"></param>
        public async Task UpdateAsync(Author author)
        {
            _dataContext.Authors.Update(author);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Get list authors from data base.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<List<Author>> GetAllAsync(int page, int size)
        {
            var listAuthor =await _dataContext.Authors.OrderBy(x => x.FirstName).ToListAsync();
            var items = listAuthor.Skip((page - 1) * size).Take(size).ToList();
            return items;
        }
        /// <summary>
        /// Get one author form data base, using it id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Author> GetAsync(Guid id)
        {
            return await _dataContext.Authors.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}