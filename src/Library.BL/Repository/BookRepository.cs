﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.BL.Interface;
using Library.DL;
using Library.DL.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.BL.Repository
{
    public class BookRepository :IService<Book>
    {
        private readonly DataContext _dataContext;

        public BookRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        /// <summary>
        /// Create a new essence book in data base.
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task<Book> CreateAsync(Book book)
        {
            var newBook = await _dataContext.Books.AddAsync(book);
            await _dataContext.SaveChangesAsync();
            return newBook.Entity;
        }

        /// <summary>
        /// Delete essence book from data base.
        /// </summary>
        /// <param name="book"></param>
        public async Task DeleteAsync(Book book)
        {
            _dataContext.Books.Remove(book);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Update essence book in data base.
        /// </summary>
        /// <param name="book"></param>
        public async Task UpdateAsync(Book book)
        {
            _dataContext.Books.Update(book);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Get all list of books from data base.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<List<Book>> GetAllAsync(int page, int size)
        {
            var listBook = await _dataContext.Books.OrderBy(x => x.PublicationDate).ToListAsync();
            var items = listBook.Skip((page - 1) * size).Take(size).ToList();
            return items;
        }
        /// <summary>
        /// Get one essence , using it id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Book> GetAsync(Guid id)
        {
            return await _dataContext.Books.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}