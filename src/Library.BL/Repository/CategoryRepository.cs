﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.BL.Interface;
using Library.DL;
using Library.DL.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.BL.Repository
{
    public class CategoryRepository : IService<Category>
    {
        private readonly DataContext _dataContext;

        public CategoryRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        /// <summary>
        /// Create new essence category.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public async Task<Category> CreateAsync(Category category)
        {
            var newCategory = await _dataContext.Categories.AddAsync(category);
            await _dataContext.SaveChangesAsync();
            return newCategory.Entity;
        }

        /// <summary>
        /// Delete essence category ferom data base.
        /// </summary>
        /// <param name="category"></param>
        public async Task DeleteAsync(Category category)
        {
            _dataContext.Categories.Remove(category);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Edit essence category in data base.
        /// </summary>
        /// <param name="category"></param>
        public async Task UpdateAsync(Category category)
        {
            _dataContext.Categories.Update(category);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Get all list of categories from data base.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<List<Category>> GetAllAsync(int page, int size)
        {
            var listCategory = await _dataContext.Categories.OrderBy(x => x.Name).ToListAsync();
            var items = listCategory.Skip((page - 1) * size).Take(size).ToList();
            return items;
        }
        /// <summary>
        /// Get one essence category from data base, using it id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Category> GetAsync(Guid id)
        {
            return await _dataContext.Categories.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}