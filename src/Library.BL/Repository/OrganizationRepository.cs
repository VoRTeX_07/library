﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.BL.Interface;
using Library.DL;
using Library.DL.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.BL.Repository
{
    public class OrganizationRepository : IService<Organization>
    {
        private readonly DataContext _dataContext;

        public OrganizationRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        /// <summary>
        /// Create new essence organization.
        /// </summary>
        /// <param name="organization"></param>
        /// <returns></returns>
        public async Task<Organization> CreateAsync(Organization organization)
        {
            var newOrganization = await _dataContext.Organizations.AddAsync(organization);
            await _dataContext.SaveChangesAsync();
            return newOrganization.Entity;
        }

        /// <summary>
        /// Delete essence organization from data base.
        /// </summary>
        /// <param name="organization"></param>
        public async Task DeleteAsync(Organization organization)
        {
            _dataContext.Organizations.Remove(organization);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Edit essence organization in data base.
        /// </summary>
        /// <param name="organization"></param>
        public async Task UpdateAsync(Organization organization)
        {
            _dataContext.Organizations.Update(organization);
            await _dataContext.SaveChangesAsync();
        }
        /// <summary>
        /// Get all list of organizations from data base.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<List<Organization>> GetAllAsync(int page, int size)
        {
            var listOrganization = await _dataContext.Organizations.OrderBy(t => t.Title).ToListAsync();
            var items = listOrganization.Skip((page - 1) * size).Take(size).ToList();
            return items;
        }
        /// <summary>
        /// Get one organization from data base, using it id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Organization> GetAsync(Guid id)
        {
            return await _dataContext.Organizations.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}