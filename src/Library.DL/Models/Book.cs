﻿using System;
using System.Collections.Generic;

namespace Library.DL.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Book
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The field Title is required, max length 100 symbols.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The page count cam be null.
        /// </summary>
        public int PagesCount { get; set; }
        /// <summary>
        /// The field publication date can be null.
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// The field create date can be null.
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// The field price can be mull.
        /// </summary>
        public int Price { get; set; }
        
        public virtual ICollection<Author> Authors { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }

    }
}