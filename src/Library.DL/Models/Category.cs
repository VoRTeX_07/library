﻿using System;
using System.Collections.Generic;

namespace Library.DL.Models
{
    public class Category
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The field name is required, max length 100 symbols.
        /// </summary>
        public  string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}