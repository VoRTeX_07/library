﻿using System;
using System.Collections.Generic;

namespace Library.DL.Models
{
    public class Organization
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The field Title is required, max lenght 100 symbols.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The field description can be null, max length 500 symbols.
        /// </summary>
        public string Description { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}