﻿using System;
using System.Collections.Generic;

namespace Library.DL.Models
{
    public class Author
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The field First name is required, max length 50 symbols.
        /// </summary>
        public  string FirstName { get; set; }
        /// <summary>
        /// The field Second name is required, max lenght 50 symbols.
        /// </summary>
        public string SecondName { get; set; }
        /// <summary>
        /// The field Middle name is required, max lenght 50 symbols.
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// The field Biography can be null, max length 500 symbols.
        /// </summary>
        public string Biography { get; set; }
        
        public virtual ICollection<Book> Books { get; set; }
    }
}