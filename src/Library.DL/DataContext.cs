﻿using Library.DL.Models;
using Microsoft.EntityFrameworkCore;


namespace Library.DL
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
           : base(options)
       {
       }
        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Author> Authors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Book>()
                .HasMany(p => p.Authors)
                .WithMany(p => p.Books);


            modelBuilder.Entity<Book>()
                .HasMany(p => p.Organizations)
                .WithMany(p => p.Books);

            modelBuilder.Entity<Book>()
                .HasMany(p => p.Categories)
                .WithMany(p => p.Books);

            modelBuilder.Entity<Author>()
                .Property(a => a.FirstName)
                .IsRequired()
                .HasMaxLength(50);
            modelBuilder.Entity<Author>()
                .Property(a => a.SecondName)
                .IsRequired()
                .HasMaxLength(50);
            modelBuilder.Entity<Author>()
                .Property(a => a.MiddleName)
                .HasMaxLength(50);
            modelBuilder.Entity<Author>()
                .Property(a => a.Biography)
                .HasMaxLength(500);

            modelBuilder.Entity<Organization>()
                .Property(o => o.Title)
                .IsRequired()
                .HasMaxLength(50);
            modelBuilder.Entity<Organization>()
                .Property(o => o.Description)
                .HasMaxLength(500);
            
            modelBuilder.Entity<Category>()
                .Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(100);      
        }
    }
    
}